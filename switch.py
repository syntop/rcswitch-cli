#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import requests

CONFIG = {
    'SERVER_URL': 'http://panel.syntop.io',
}

def usage(switches=()):
    print >> sys.stderr, "Usage: {0} <switchname>[,<switchname>,...] [on|off]\n  e.g. {0} espresso on\n".format(sys.argv[0])

    # Print switches if available
    if len(switches):
        print >> sys.stderr, 'Available switches:'
        print >> sys.stderr, '   all'
        for switch in switches:
            print >> sys.stderr, u'\uD83D\uDD34' if switch['state'] else u'\u26AB\uFE0F',
            print >> sys.stderr, ' {0}'.format(switch['label'].lower().replace(' ', ''))
    else:
        # Switches will be shown with help flag
        print >> sys.stderr, "Help: {0} [--help|/?]".format(sys.argv[0])

    sys.exit(2)

def main():

    # Get switches config from server
    try:
        resp = requests.get("{SERVER_URL}/switches".format(**CONFIG))
        resp.raise_for_status()
    except requests.exceptions.RequestException: # BaseException
        print >> sys.stderr, 'Error: Connection problem.'
        sys.exit(1)

    switches = resp.json().get('switches', [])

    if len(sys.argv) == 3:
        if sys.argv[2].lower() not in ('on', 'off', ):
            usage(switches)
        state = 1 if sys.argv[2] == 'on' else 0

    # Wrong arguments or help requested
    elif len(sys.argv) not in (2,3,) or (len(sys.argv) == 2 and sys.argv[1].lower() in ('--help', '/?')):
        usage(switches)

    else:
        state = None

    switchnames = sys.argv[1].decode('utf-8').lower().split(',')

    # Determine which switch(es) should be switched
    selected_switches = []
    for switch in switches:
        label = switch['label'].lower().replace(' ','')
        for switchname in switchnames:
            if label.startswith(switchname) or switchname == 'all' or (switchname.isdigit() and switch['id'] == int(switchname)):
                if state is None:
                    to_state = abs(switch['state'] - 1)
                else:
                    to_state = state
                selected_switches.append((switch['id'], switch['label'], to_state))

    # Check if we found any switches
    if len(selected_switches) == 0:
        print >> sys.stderr, u"Sorry, no switches found for «{0}».".format(",".join(switchnames))
        sys.exit(2)

    # Do the switching
    for switch_id, label, state in selected_switches:
        print u"Switching {0} {1} ...".format(label, ["off","on"][state]),
        try:
            resp = requests.post("{0}/switches/{1}".format(CONFIG['SERVER_URL'], switch_id), data={"state": state})
            resp.raise_for_status()
        except:
            print >> sys.stderr, " Failed."
        else:
            print "Done."

if __name__ == "__main__":
    main()
