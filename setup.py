#!/usr/bin/env python

from setuptools import setup

setup(
    name='Switch',
    version='1.3',
    description='CLI interface for SYNTOP switch panel',
    author='Philipp Bosch',
    author_email='philipp@syntop.io',
    url='https://bitbucket.org/syntop/rcswitch-cli',
    py_modules=['switch'],
    install_requires=['requests>=1.1'],
    zip_safe=False,
    entry_points = {
        'console_scripts': [
            'switch = switch:main',
        ]
    },
)
